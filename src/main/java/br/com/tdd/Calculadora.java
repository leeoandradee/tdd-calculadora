package br.com.tdd;

public class Calculadora {

    public static int soma(int valor1, int valor2) {
        return valor1 + valor2;
    }

    public static double soma(double valor1, double valor2) {
        return valor1 + valor2;
    }

    public static int subtrair(int valor1, int valor2) {
        return valor1 - valor2;
    }

    public static double subtrair(double valor1, double valor2) {
        return valor1 - valor2;
    }

    public static int dividir(int valor1, int valor2) {
        return valor1/valor2;
    }

    public static double dividir(double valor1, double valor2) {
        return valor1/valor2;
    }

    public static int multiplicar(int valor1, int valor2) {
        return valor1*valor2;
    }

    public static double multiplicar(double valor1, double valor2) {
        return valor1*valor2;
    }
}
