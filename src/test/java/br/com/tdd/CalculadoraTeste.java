package br.com.tdd;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

public class CalculadoraTeste {

    @Test
    public void testarSomaNumeroInteiro() {
        int resultado = Calculadora.soma(2, 2);
        Assertions.assertEquals(4, resultado);
    }

    @Test
    public void testarSomaNumeroFlutuente() {
        double resultado = Calculadora.soma(2.2, 2.2);
        Assertions.assertEquals(4.4, resultado);
    }

    @Test
    public void testarSubtracaoNumeroInteiro() {
        int resultado = Calculadora.subtrair(2, 2);
        Assertions.assertEquals(0, resultado);
    }

    @Test
    public void testarSubtracaoNumeroFlutuante() {
        double resultado = Calculadora.subtrair(2.2, 2.2);
        Assertions.assertEquals(0, resultado);
    }

    @Test
    public void testarDivisaoNumeroInteiro() {
        int resultado = Calculadora.dividir(2, 2);
        Assertions.assertEquals(1, resultado);
    }

    @Test
    public void testarDivisaoNumeroFlutuante() {
        double resultado = Calculadora.dividir(2.2, 2.2);
        Assertions.assertEquals(1, resultado);
    }

    @Test
    public void testarMultiplicacaoNumeroInteiro() {
        int resultado = Calculadora.multiplicar(2, 2);
        Assertions.assertEquals(4, resultado);
    }

    @Test
    public void testarMultiplicacaoNumeroFlutuante() {
        double resultado = Calculadora.multiplicar(2.0, 2.0);
        Assertions.assertEquals(4.0, resultado);
    }

}
